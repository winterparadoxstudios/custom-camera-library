package com.winterparadox.customcameralibrary;

/**
 * Created by Adeel on 12/8/2015.
 */
public class CustomCamera {

    public static final String ACTION_IMAGE_CAPTURE = "com.winterparadox.customcameralibrary" +
            ".IMAGE_CAPTURE";
    public static final String ACTION_VIDEO_CAPTURE = "com.winterparadox.customcameralibrary" +
            ".VIDEO_CAPTURE";

    public static int QUALITY_720 = 720;
    public static int QUALITY_1080 = 720;
    public static int QUALITY_360 = 360;


}
