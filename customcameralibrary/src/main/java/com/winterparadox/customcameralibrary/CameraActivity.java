package com.winterparadox.customcameralibrary;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Adeel on 3/26/2015.
 */
public class CameraActivity extends Activity implements View.OnClickListener,ScaleGestureDetector
        .OnScaleGestureListener, View.OnTouchListener, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "Camera";
    private static final int PERMISSION_REQUEST = 12;
    private static final String FIX = "fix";

    private Camera camera;
    private CameraPreview mPreview;
    private String outputFile;
    private ImageButton btnSave;
    private boolean isPictureTaken;
    private MediaRecorder mMediaRecorder;
    private boolean isRecording = false;
    private TextView txtIndicator;
    private Button btnRecord;
    private CheckBox chkImageFix;
    private ScaleGestureDetector mScaleDetector;
    private TextView txtZoom;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if ( hasPermission (Manifest.permission.WRITE_EXTERNAL_STORAGE)
                && hasPermission (Manifest.permission.CAMERA)
                && hasPermission (Manifest.permission.RECORD_AUDIO)) {

            startCamera ();

        } else {

            ActivityCompat.requestPermissions (this, new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO}, PERMISSION_REQUEST);

        }

    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[]
            grantResults) {

        if (requestCode == PERMISSION_REQUEST) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] ==
                    PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager
                    .PERMISSION_GRANTED) {
                startCamera ();
            } else {
                exit (R.string.tstNoPermission);
            }


        }

        super.onRequestPermissionsResult (requestCode, permissions, grantResults);
    }

    private void startCamera () {
        if ( !checkCameraHardware(this) ) {
            exit(R.string.tstNoCamera);
            return;
        }

        camera = getCameraInstance();

        if ( camera == null ) {
            exit(R.string.tstNoCamera);
            return;
        }

        setContentView(R.layout.activity_camera);

        outputFile = getIntent().getStringExtra(MediaStore.EXTRA_OUTPUT);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences (this);

        if ( outputFile == null ) {
            exit(R.string.tstNoPath);
            return;
        }

        FrameLayout previewContainer = (FrameLayout) findViewById (R.id.camera_preview);
        Button btnCapture = (Button) findViewById(R.id.btnCapture);
        btnRecord = (Button) findViewById(R.id.btnRecord);
        chkImageFix = (CheckBox) findViewById(R.id.chkbxImageFix);
        ImageButton btnClose = (ImageButton) findViewById (R.id.btnClose);
        btnSave = (ImageButton) findViewById(R.id.btnSave);
        txtIndicator = (TextView) findViewById(R.id.txtVideoIndicator);
        txtZoom = (TextView) findViewById(R.id.txtZoom);

        boolean aBoolean = sharedPreferences.getBoolean (FIX, false);
        chkImageFix.setChecked (aBoolean);

        if (getIntent().getAction().equals(CustomCamera.ACTION_IMAGE_CAPTURE)) {
            mPreview = new CameraPreview (this, camera, false);
            mPreview.setImageFix (aBoolean);

            previewContainer.addView(mPreview);
            btnRecord.setVisibility(View.INVISIBLE);
            txtIndicator.setVisibility(View.INVISIBLE);

        } else if (getIntent().getAction().equals(CustomCamera.ACTION_VIDEO_CAPTURE)) {
            mPreview = new CameraPreview(this, camera, true);
            mPreview.setImageFix (aBoolean);

            previewContainer.addView(mPreview);

            btnCapture.setVisibility(View.INVISIBLE);
//            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
//                txtIndicator.setRotation (90);
//
//                btnSave.setPivotX(dpToPx (this, 55));
//                btnSave.setPivotY(dpToPx (this, 55));
//                btnSave.setRotation (90);
//                chkImageFix.setRotation (90);
//                txtZoom.setRotation (90);
//            }
        }

        btnSave.setEnabled(false);
        chkImageFix.setEnabled (!btnSave.isEnabled ());

        chkImageFix.setOnCheckedChangeListener (this);
        btnCapture.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        btnRecord.setOnClickListener(this);
        previewContainer.setOnTouchListener (this);
        mScaleDetector = new ScaleGestureDetector (this, this);

    }

    private boolean hasPermission (String camera) {
        return ContextCompat.checkSelfPermission (this,
                camera) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        switch (((WindowManager) getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_90:
                mPreview.setOrientation (CameraPreview.LANDSCAPE);
                Log.d("camview", "landscape");
                break;
            case Surface.ROTATION_180:
            case Surface.ROTATION_0:
                mPreview.setOrientation (CameraPreview.PORTRAIT);
                Log.d("camview", "port");
                break;
            case Surface.ROTATION_270:
                mPreview.setOrientation (CameraPreview.REVERSE_LANDSCAPE);
                Log.d("camview", "reverse");
                break;
        }
    }

    // __________________ CAMERA RELATED FUNCTION _______________

    private boolean checkCameraHardware(Context context) {
        // this device has a camera
        // no camera on this device
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private boolean prepareVideoRecorder(Camera mCamera){

        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
//        profile.videoFrameWidth = 512;
//        profile.videoFrameHeight = 288;

        profile.fileFormat = MediaRecorder.OutputFormat.DEFAULT;
        mMediaRecorder.setProfile(profile);



        // Step 4: Set output file
        mMediaRecorder.setOutputFile(new File(outputFile).toString());

        // Step 5: Set the preview output
        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder(mCamera);
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder(mCamera);
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder(Camera mCamera){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private byte[] pictureData;
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            camera.stopPreview();
            isPictureTaken = true;
            pictureData = data;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnSave.setEnabled(true);
                    chkImageFix.setEnabled (!btnSave.isEnabled ());
                }
            });
        }
    };

    void toggleRecording() {
        if (isRecording) {
            isRecording = false;

            // stop recording and release camera
            mMediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(camera); // release the MediaRecorder object
            camera.lock();         // take camera access back from MediaRecorder

            // inform the user that recording has stopped
//            setCaptureButtonText("Capture");

            camera.stopPreview();
            isPictureTaken = true;
            btnSave.setEnabled(true);
            chkImageFix.setEnabled (!btnSave.isEnabled ());
            btnRecord.setSelected(false);
        } else {
            // initialize video camera
            if (prepareVideoRecorder(camera)) {
                // Camera is available and unlocked, MediaRecorder is prepared,
                // now you can start recording
                mMediaRecorder.start();

                startIndicator();
                // inform the user that recording has started
//                setCaptureButtonText("Stop");
                isRecording = true;
                btnRecord.setSelected(true);
            } else {
                // prepare didn't work, release the camera
                releaseMediaRecorder(camera);
                // inform user
            }
        }
    }

    void savePicture(byte[] data) {
        File pictureFile = new File(outputFile);
        if (pictureFile == null){
            Log.d(TAG, "Error creating media file, check storage permissions: ");
            return;
        }

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }

        setResult(RESULT_OK);
        finish();
    }

//    ________________________ Override __________________

    @Override
    public void onClick(View v) {
        int i = v.getId ();
        if ( i == R.id.btnCapture ) {
            if ( !isPictureTaken ) {
                camera.takePicture (null, null, mPicture);
                MediaPlayer mMediaPlayer = MediaPlayer.create (this, R.raw.camera_sound);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.start();
            }

        } else if ( i == R.id.btnClose ) {
            if ( isPictureTaken ) {
                btnSave.setEnabled (false);
                chkImageFix.setEnabled (!btnSave.isEnabled ());
                camera.startPreview ();
                isPictureTaken = false;
            } else {
                finish ();
            }

        } else if ( i == R.id.btnSave ) {
            if ( getIntent ().getAction ().equals (CustomCamera.ACTION_IMAGE_CAPTURE) ) {
                savePicture (pictureData);
            } else if ( getIntent ().getAction ().equals (CustomCamera.ACTION_VIDEO_CAPTURE) ) {
                setResult (RESULT_OK);
                finish ();
            }

        } else if ( i == R.id.btnRecord ) {
            if ( !isPictureTaken ) {
                toggleRecording ();
            }

        }
    }

    void exit(int msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (btnSave != null) {
            btnSave.setEnabled (false);
            chkImageFix.setEnabled (true);
        }
    }

    @Override
    protected void onDestroy() {
        if (camera != null) {
            camera.release ();
        }
        super.onDestroy();
    }

//    __________________ INDICATOR HELPER ___________________

    public void startIndicator() {

        txtIndicator.setText("00:00");
        new Thread(new Runnable() {
            @Override
            public void run() {
                int seconds = 0;
                while (isRecording) {

                    SystemClock.sleep(1000);
                    if (!isRecording) return;
                    seconds += 1;
                    final String time = addZero(seconds / 60) +
                            ":" + addZero(seconds % 60);

                    final int finalSeconds = seconds;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtIndicator.setText(String.format ("%s", time));

                            if (finalSeconds >= 60) {
                                toggleRecording();
                            }
                        }
                    });


                }
            }
        }).start();
    }

    String addZero(int n) {
        String s = n+"";
        if (n < 10)
            s = "0" + n;
        return s;
    }

    @Override
    public boolean onScale (ScaleGestureDetector detector) {
        Log.e ("zoom", "on scale end");
        Log.e ("zoom", "on scale = " + detector.getScaleFactor ());
        Camera.Parameters parameters = camera.getParameters ();
        if (!isPictureTaken && parameters.isZoomSupported ()) {

            int zoom = parameters.getZoom ();
            int maxZoom = parameters.getMaxZoom ();

            if (detector.getScaleFactor () < 0.9) {
                zoom--;
            } else if (detector.getScaleFactor () > 1.1) {
                zoom++;
            }

            if (zoom > 0 && zoom <= maxZoom) {

                parameters.setZoom (zoom);
                txtZoom.setText (String.format ("x%s", zoom));
                camera.setParameters (parameters);

            }
        }
        return true;
    }

    @Override
    public boolean onScaleBegin (ScaleGestureDetector detector) {
        Log.e ("zoom", "on scale begin");

        return true;
    }

    @Override
    public void onScaleEnd (ScaleGestureDetector detector) {

    }

    @Override
    public boolean onTouch (View v, MotionEvent event) {
        return mScaleDetector.onTouchEvent (event);
    }

    public static int dpToPx(Context c, int dp) {
        DisplayMetrics displayMetrics = c.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Override
    public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
        mPreview.setImageFix (isChecked);
        Log.d (TAG, "fix called");
        SharedPreferences.Editor edit = sharedPreferences.edit ();
        edit.putBoolean (FIX, isChecked);
        edit.apply ();
    }
}
