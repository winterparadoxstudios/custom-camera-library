package com.winterparadox.customcameralibrary;

/**
 * Created by Adeel on 3/26/2015.
 */

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "CamView";
    public static final int PORTRAIT = 0;
    public static final int LANDSCAPE = 1;
    public static final int REVERSE_LANDSCAPE = 2;

    private final boolean mVideo;
    private SurfaceHolder mHolder;
    private int orientation; // 0 = portrait, 1 = landscape, 2 = reverse landscape
    private Camera mCamera;
    private boolean imageFix;

    public CameraPreview(Context context, Camera camera, boolean video) {
        super(context);
        mCamera = camera;
        mVideo = video;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setSizeFromLayout ();
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
//            SurfaceTexture st = new SurfaceTexture (Activity.MODE_PRIVATE);
//            mCamera.setPreviewTexture(st);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void setOrientation (int orientation) {
        this.orientation = orientation;
        Log.d(TAG, " orientation set = " + orientation);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        Log.d(TAG, "w " + w + " h = " + h + " orientation = " + orientation);

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
//        int height = 720;
        boolean rotate = false;
        int height;
        int width;

        float ratio;

        if (w < h) {
            ratio = (h * 1f) / (w * 1f) ;
            rotate = true;
            height = CustomCamera.QUALITY_720;   // default quality = 720
            width = (int) (CustomCamera.QUALITY_720 * ratio);
        } else {
            ratio = (w * 1f) / (h * 1f) ;
            height = CustomCamera.QUALITY_720;
            width = (int) (CustomCamera.QUALITY_720 * ratio);
        }

        // start preview with new settings
        try {
            Camera.Parameters params = mCamera.getParameters();

            List<Camera.Size> sizes = params.getSupportedPreviewSizes ();

            Camera.Size theClosestSize = getTheClosestSize (width, sizes);

            width = theClosestSize.width;
            height = theClosestSize.height;

            if (!mVideo) {
                params.setPictureSize(width, height);
                params.setPreviewSize(width, height);
                Log.d(TAG, "w " + width + " h = " + height );

                if (!imageFix) {
                    if ( rotate ) {
                        params.setRotation (0);
                        params.setRotation (90);
                    } else {
                        params.setRotation (0);
                        if ( orientation == REVERSE_LANDSCAPE ) {

                            params.setRotation (180);
                        }
                    }

                } else {
                    if ( rotate ) {
                        params.setRotation (0);
                        params.setRotation (270);
                    } else {
                        params.setRotation (180);
                        if ( orientation == REVERSE_LANDSCAPE ) {

                            params.setRotation (0);
                        }
                    }
                }

            } else {
                params.setPictureSize(width, height);
                params.setPreviewSize(width, height);

                if (!imageFix) {
                    if ( rotate ) {
                        params.setRotation (0);
                        params.setRotation (90);
                    } else {
                        params.setRotation (0);
                        if ( orientation == REVERSE_LANDSCAPE ) {

                            params.setRotation (180);
                        }
                    }
                } else {

                    if ( rotate ) {
                        params.setRotation (0);
                        params.setRotation (270);
                    } else {
                        params.setRotation (180);
                        if ( orientation == REVERSE_LANDSCAPE ) {

                            params.setRotation (0);
                        }
                    }
                }
            }
            if (mVideo && Build.VERSION.SDK_INT >= 14) {
                params.setRecordingHint(true);
            }

            mCamera.setParameters(params);

            if (!imageFix) {
                if ( rotate ) {
                    mCamera.setDisplayOrientation (90);
                } else {
                    mCamera.setDisplayOrientation (0);
                    if ( orientation == REVERSE_LANDSCAPE ) {

                        mCamera.setDisplayOrientation (180);
                    }
                }
            } else {

                if ( rotate ) {
                    mCamera.setDisplayOrientation (270);
                } else {
                    mCamera.setDisplayOrientation (180);
                    if ( orientation == REVERSE_LANDSCAPE ) {

                        mCamera.setDisplayOrientation (0);
                    }
                }
            }
            mCamera.setPreviewDisplay(mHolder);
//            SurfaceTexture st = new SurfaceTexture (Activity.MODE_PRIVATE);
//            mCamera.setPreviewTexture(st);
            mCamera.startPreview();

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    private static Camera.Size getTheClosestSize (int width, List<Camera.Size> sizes) {

        Log.d(TAG, "list of sizes = " + sizes.size ());

        int smallestDiff = Integer.MAX_VALUE;
        Camera.Size finalSize = null;
        for ( int i = 0; i < sizes.size (); i++ ) {
            Camera.Size size = sizes.get (i);


            finalSize = Math.abs (size.width - width) < smallestDiff ? size : finalSize;
            smallestDiff = Math.abs (size.width - width) < smallestDiff ? Math.abs (size.width -
                    width) : smallestDiff;

        }

        return finalSize;

    }

    public void setImageFix (boolean imageFix) {
        this.imageFix = imageFix;
        setVisibility (INVISIBLE);
        setVisibility (VISIBLE);
    }
}