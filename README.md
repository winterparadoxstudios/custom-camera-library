A library that opens a custom camera activity with intent.

Check the sample for usage.

To use the library, include following line to your build.gradle

    compile 'com.winterparadox:customcameralibrary:0.3.1'