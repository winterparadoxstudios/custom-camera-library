package com.winterparadox.samplecustomcamera;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.winterparadox.customcameralibrary.CustomCamera;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_IMAGE = 12;
    private File image;
    private SimpleDraweeView draweeView;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        Fresco.initialize (this);

        setContentView (R.layout.activity_main);

        draweeView = (SimpleDraweeView) findViewById (R.id.my_image_view);
        View button = findViewById (R.id.button);

        button.setOnClickListener (this);


    }

    @Override
    public void onClick (View v) {
        if (v.getId () == R.id.button) {

            // path to save the image file; remember to add read/write storage permissions to
            // manifest
            image = new File (Environment.getExternalStorageDirectory(), "sample.jpg");

            // need RECORD_AUDIO and CAMERA permissions
//            Intent intent = new Intent (CustomCamera.ACTION_IMAGE_CAPTURE);

//            use the following intent to record videos
            Intent intent = new Intent (CustomCamera.ACTION_VIDEO_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, image.getPath ());
            startActivityForResult (intent, REQUEST_CODE_IMAGE);

        }
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult (requestCode, resultCode, data);

        if( requestCode == REQUEST_CODE_IMAGE && resultCode == Activity.RESULT_OK ){

            // just clearing the cache; else same image appears everytime
            ImagePipeline imagePipeline = Fresco.getImagePipeline();

            imagePipeline.clearCaches();

            draweeView.setImageURI (Uri.fromFile (image));
        }
    }
}
